from django.db import models

# Create your models here.
class Pais(models.Model):
    codigo_iso2 = models.CharField(max_length=2)
    codigo_iso3 = models.CharField(max_length=2)
    descripcion = models.CharField(max_length=32)
    activo = models.BooleanField
    fecha_insercion = models.DateTimeField
    usuario_insercion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField
    usuario_modificacion = models.CharField(max_length=16)

    def __str__(self):
        return f'Pais {self.id}:{self.codigo_iso2} {self.codigo_iso3} {self.descripcion}'


class Departamento(models.Model):
    codigo = models.SmallIntegerField
    descripcion = models.CharField(max_length=32)
    activo = models.BooleanField
    fecha_insercion = models.DateTimeField
    usuario_insercion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField
    usuario_modificacion = models.CharField(max_length=16)
    pais_id = models.ForeignKey(Pais, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return f'Departamento {self.id}: {self.descripcion}'

class Distrito(models.Model):
    codigo = models.SmallIntegerField
    descripcion = models.CharField(max_length=32)
    activo = models.BooleanField
    fecha_insercion = models.DateTimeField
    usuario_insercion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField
    usuario_modificacion = models.CharField(max_length=16)
    departamento_id = models.ForeignKey(Departamento, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return f'Distrito {self.id}: {self.descripcion}'


class Persona(models.Model):
    numero_documento = models.CharField(max_length=16)
    nombre = models.CharField(max_length=64)
    apellido = models.CharField(max_length=64)
    fecha_nacimiento = models.DateField
    direccion = models.CharField(max_length=70)
    telefono = models.CharField(max_length=20)
    email = models.CharField(max_length=50)
    activo = models.BooleanField
    fecha_insercion = models.DateTimeField
    usuario_insercion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField
    usuario_modificacion = models.CharField(max_length=16)
    distrito_id = models.ForeignKey(Distrito, on_delete=models.SET_NULL, null=True)



    def __str__(self):
        return f'Persona {self.id}: {self.nombre} {self.apellido} {self.numero_documento}'
