from django.contrib import admin

from personas.models import Persona, Distrito, Departamento, Pais

# Register your models here.
admin.site.register(Persona)
admin.site.register(Distrito)
admin.site.register(Departamento)
admin.site.register(Pais)